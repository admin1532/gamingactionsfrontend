import React from 'react';
import { StatusBar, StyleSheet, View } from 'react-native';
import Navigation from './navigation';
import axios from 'axios';
import AsyncStorage from '@react-native-async-storage/async-storage';
axios.defaults.baseURL ="https://52d3-197-149-242-161.ngrok.io";
axios.defaults.headers.post["Content-Type"] = "application/json";
axios.defaults.headers.post["Accept"] = "application/json";
axios.defaults.withCredentials = true;

// const renderInterce = async() => {
// axios.interceptors.request.use(function async (config) {
//   const token = await AsyncStorage.getItem('token');
//   config.headers.Authorization = token ? `Bearer ${token}` : '';
//   return config;
// });
// }

export default function App() {
  return (
      <View style={{ flex:1, backgroundColor:'black' }}>
        <Navigation />
         <StatusBar barStyle='black-content' />

      </View>
  );
}

const styles = StyleSheet.create({
  container: {
  },
});
